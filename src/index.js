function MyArray(arg, ...args) {
  if (!new.target) {
    return new MyArray(arg, ...args);
  }

  Object.defineProperty(this, '_length', { value: arguments.length,
    writable: true,
    enumerable: false }
  );

  Object.defineProperty(this, 'length', {
    enumerable: false,
    set(lengtSet) {
      if (typeof lengtSet !== 'number') {
        throw new RangeError('Invalid array length');
      }

      if (lengtSet < this.length) {
        for (let i = lengtSet; i < this.length; i++) {
          delete this[i];
        }
      }
      this._length = lengtSet;
    },
    get() {
      return this._length;
    }
  });

  if (args.length === 0 && arg === undefined) {
    this.length = 0;
    return this;
  }

  if (args.length === 0 && typeof arg === 'number' && Number.isNaN(arg)) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 0 && arg >= (2 ** 32)) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 0 && arg < 0) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 0 && typeof arg === 'number') {
    this.length = arg;
    return this;
  }

  this[0] = arg;

  for (let i = 0; i < args.length; i++) {
    this[i + 1] = args[i];
  }

  this.length = arguments.length;

  const currentLength = this.length;

  const prox = new Proxy(this, {
    get(target, prop) {
      return target[prop];
    },
    set(target, prop, value) {
      if (prop > target.length) {
        target[prop] = value;
        target.length += 1;

        for (let i = prop - 1; i >= currentLength; i--) {
          target[i] = undefined;
          target.length += 1;
        }
      } else if (prop < 0 || prop <= target.length || isNaN(prop)) {
        target[prop] = value;
      } else {
        target[prop] = value;
        target.length += 1;
      }
    }
  });

  return prox;
}


MyArray.prototype[Symbol.iterator] = function() {
  let counter = -1;

  return {
    next: () => {
      counter += 1;

      if (counter < this.length) {
        return {
          value: this[counter],
          done: false
        };
      } else {
        return {
          done: true
        };
      }
    }
  };
};

MyArray.prototype.map = function(callback, ...context) {
  const thisArgCallBack = callback.bind(context[0]);
  const newArr = new MyArray(this.length);

  for (let i = 0; i < newArr.length; i++) {
    if (this[i] === undefined) {
      i += 1;
    }

    if (i in this) {
      newArr[i] = thisArgCallBack(this[i], i, this);
    }
  }

  return newArr;
};

MyArray.prototype.reduce = function(callback, ...initValue) {
  let inValue = this[0];

  if (arguments.length > 1) {
    inValue = initValue[0];
  }

  const curLength = this.length;

  if (inValue === undefined && !curLength) {
    throw new TypeError();
  }

  if (inValue === this[0]) {
    for (let i = 1; i < curLength; i++) {
      if (i in this) {
        inValue = callback(inValue, this[i], i, this);
      }
    }

    return inValue;
  }

  for (let i = 0; i < curLength; i++) {
    if (i in this) {
      inValue = callback(inValue, this[i], i, this);
    }
  }

  return inValue;
};

MyArray.prototype.forEach = function(func, context = this) {
  const newFunc = new MyArray(this.length);
  const callback = func.bind(context);

  for (let i = 0; i < newFunc.length; i++) {
    if (i in this) {
      callback(this[i], i, this);
    }
  }
};

MyArray.prototype.filter = function(func, ...context) {
  const newObj = new MyArray();
  const currLength = this.length;
  const callback = func.bind(context[0]);

  for (let i = 0; i < currLength; i++) {
    if (i in this) {
      const currValue = this[i];

      if (callback(this[i], i, this)) {
        newObj[newObj.length] = currValue;
        newObj.length += 1;
      }
    }
  }
  return newObj;
};

MyArray.prototype.push = function(arg, ...args) {
  this[this.length] = arg;
  this.length += 1;

  if (!arguments.length && !this.length) {
    this.length = 0;
  }

  if (!Number.isInteger(this.length)) {
    this[0] = arg;
    this.length = 1;

    for (let i = 1; i < args.length + 1; i++) {
      this[i] = args[i];
      this.length = i;
    }
    return this.length;
  }

  for (let i = 0; i < args.length; i++) {
    this[this.length] = args[i];
    this.length += 1;
  }
  return this.length;
};

MyArray.prototype.pop = function() {
  if (!this.length) {
    this.length = 0;
  }

  if (this.length === 0) {
    return undefined;
  }

  const item = this[this.length - 1];
  delete this[this.length - 1];
  this.length -= 1;
  return item;
};

MyArray.prototype.toString = function() {
  let retString = '';

  for (let i = 0; i < this.length; i++) {
    retString += `${this[i] || ''},`;
  }
  return retString.slice(0, -1);
};

MyArray.prototype.sort = function(callback) {
  const currLength = this.length;
  const newArr = new MyArray(0);
  let counter = 0;

  if (!callback) {
    for (let x = this.length - 1; x > 0; x--) {
      for (let i = 0; i < x; i++) {
        if (`${this[i]}` > `${this[i + 1]}`) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }

  for (let i = 1; i < currLength; i++) {
    if (i in this) {
      newArr[counter] = callback(this[i], this[i - 1]);
      counter += 1;
      newArr.length += 1;
    }
  }

  for (let i = 0; i < newArr.length; i++) {
    if (!callback.length && newArr[i] === -1) {
      const copyThis = new MyArray(...this);
      let index = 0;

      for (let i = copyThis.length - 1; i >= 0; i--) {
        this[index] = copyThis[i];
        index += 1;
      }
      return this;
    }
  }

  if (callback[0] > 0) {
    for (let x = this.length - 1; x > 0; x--) {
      for (let i = 0; i < x; i++) {
        if (this[i] > this[i + 1]) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }

  if (callback[0] < 0) {
    for (let x = this.length - 1; x > 0; x--) {
      for (let i = 0; i < x; i++) {
        if (this[i] < this[i + 1]) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }
};

MyArray.prototype.splice = function(start, end, ...args) {
  let startVal = Math.floor(start);
  let endVal = end;
  let counter = 0;

  const newArray = new MyArray();

  if (endVal === undefined) {
    endVal = this.length;
  }

  if (endVal >= (this.length - startVal)) {
    endVal = this.length - startVal;
  }

  if (startVal < 0) {
    startVal += this.length;
  }

  if (isNaN(startVal) && isNaN(endVal)) {
    return { ...this };
  }

  if (isNaN(startVal)) {
    startVal = 0;
    endVal = this.length;
  }

  if (startVal === -Infinity) {
    startVal = 0;
  }

  const sumLength = startVal + endVal;

  if (args.length === 0) {
    for (let i = startVal; i < sumLength; i++) {
      newArray[counter] = this[i];
      this[i] = this[i + endVal];
      counter += 1;
    }
    this.length -= endVal;
  }

  if (args.length > 0 && endVal === 0) {
    for (let i = startVal; i < args.length + 1; i++) {
      this[i + 1] = this[i];
      this[i] = args[counter];
      counter += 1;
      this.length += 1;
    }
    this.length -= endVal;
  }

  if (args.length > 0) {
    for (let i = startVal; i < sumLength; i++) {
      newArray[counter] = this[i];
      this[i] = args[counter];
      counter += 1;
    }
    this.length -= endVal;
  }

  newArray.length = counter;

  return newArray;
};


MyArray.prototype.indexOf = function(findItem, startIndex = 0) {
  if (startIndex < 0) {
    return -1;
  }

  for (let i = startIndex; i < this.length; i++) {
    if (findItem === this[i]) {
      return i;
    }
  }
  return -1;
};

MyArray.prototype.slice = function(begin, end) {
  const currLength = this.length;
  let beginValue = !begin ? 0 : begin;
  let endValue = !end ? currLength : end;

  const newArray = new MyArray(0);
  let count = 0;

  if (beginValue < 0) {
    beginValue += currLength;
  }

  if (endValue < 0) {
    endValue += currLength;
  }

  if (endValue > currLength) {
    endValue = currLength;
  }

  if (isNaN(beginValue)) {
    for (let i = 0; i < currLength; i++) {
      newArray[i] = this[i];
    }
    newArray.length = currLength;
    return newArray;
  }

  for (let i = beginValue; i < endValue; i++) {
    newArray[count] = this[i];
    count += 1;
    newArray.length += 1;
  }

  return newArray;
};

MyArray.from = function(list, ...callback) {
  let index = 0;
  const newArr = new MyArray();
  const context = callback[1] ? callback[1] : undefined;

  if (list[0] === undefined) {
    newArr.length = 0;
    return newArr;
  }

  for (let i = 0; i < list.length; i++) {
    if (callback[0]) {
      newArr[index] = callback[0].call(context, list[i], index);
      index += 1;
    } else {
      newArr[index] = list[i];
      index += 1;
    }
  }

  newArr.length = index;
  return newArr;
};

module.exports = MyArray;